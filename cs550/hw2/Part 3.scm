; Factorial
; Computes n!
(define (factorial n)
  (if (= n 1) 1
      (* n (factorial (- n 1))
	 )
      )
  )

(define (factorial-iter n)
  (factorial-helper 1 1 n))

(define (factorial-helper current total n)
  (if (= n current) (* current total)
      (factorial-helper (+ current 1) (* current total) n)
      )
  )

; Sum
; Computes sum from 0 up to and including n
(define (sum n)
  (if (= n 0) 0
      (+ n (sum (- n 1)
		)
	 )
      )
  )

(define (sum-iter n)
  (sum-helper 1 0 n)
  )

(define (sum-helper current total n)
  (if (= current n) (+ total current)
      (sum-helper (+ current 1) (+ current total) n)
      )
  )

; Pow
; Raises a to the power b
(define (pow base exp)
  (if (= exp 1) base
      (* base (pow base (- exp 1))
	 )
      )
  )

(define (pow-iter base exp)
  (pow-helper 1 1 base exp)
  )

(define (pow-helper current total base exp)
  (if (= current exp) (* total base)
      (pow-helper (+ current 1) (* total base) base exp)
      )
  )
