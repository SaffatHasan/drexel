(define (sieve stream)
   (cons-stream
     (head stream)
     (sieve
       (filter-stream
         (lambda (x) (not (divisible? x (head stream))))
         (tail stream)))))

(define (divisible? x y)
  (= (remainder x y) 0))

(define (filter-stream p lst)
  (cond ((empty-stream? lst) the-empty-stream)
        ((p (head lst)) (cons-stream (head lst) (filter-stream p (tail lst))))
        (else (filter-stream p (tail lst)))))

(define (integers-starting-from n)
 (cons-stream n
  (integers-starting-from (+ n 1))))

(define primes (sieve (integers-starting-from 2)))

(define (get-nth-in-stream n stream)
  (if (= n 1) (stream-car stream)
      (get-nth-in-stream (- n 1) (stream-cdr stream))
      )
  )
