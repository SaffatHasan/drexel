(define (eval-and expr)
  (cond ((null? expr) #t) ; If there are no expressions then true is returned
	((null? (cdr expr)) (car expr)) ; If all the expressions evaluate to true values, the value of the last expression is returned
	((not (car expr)) #f) ; If any expression evaluate to false, false is returned; any remaining expressions are not evaluated
	(else (eval-and (cdr expr)))))

(define (eval-or expr)
  (cond ((null? expr) #f) ; If there are no expressions, then false is returned
	((null? (cdr expr)) (car expr)) ; If all the expressions evaluate to false values, the value of the last expression is returned
	((car expr) #t) ;  If any expression evaluates to a true value, that value is returned
	(else (eval-or (cdr expr)))))


(define test1 (list #f #f))
(define test2 (list #f #t))
(define test3 (list #t #f))
(define test4 (list #t #t))

(eval-and test1)
(eval-and test2)
(eval-and test3)
(eval-and test4)

(eval-or test1)
(eval-or test2)
(eval-or test3)
(eval-or test4)
