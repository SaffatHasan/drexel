; We can define a while loop as having two parts:
; An expression and a body

(define (while-expr expr) (car expr))
(define (while-body expr) (cadr expr))
(define (while? expr
