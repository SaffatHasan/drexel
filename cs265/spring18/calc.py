#!/usr/bin/python3
import sys

def is_operand(x):
  try:
    int(x)
  except ValueError:
    return False
  return True

def infix2postfix(expression):
  prec = {}
  prec["*"] = 3
  prec["/"] = 3
  prec["%"] = 3
  prec["+"] = 2
  prec["-"] = 2
  prec["("] = 1
  stack = []
  postfix = []

  for item in expression.split():
    if is_operand(item):
      postfix.append(item)
    else:
      if item == '(':
        stack.append(item)
      elif item == ')':
        operator = stack.pop()
        while operator != '(':
          postfix.append(operator)
          operator = stack.pop()
      else:
        while ((len(stack) != 0) and (prec[stack[-1]] >= prec[item])):
          postfix.append(stack.pop())
        stack.append(item)

  while stack:
    postfix.append(stack.pop())

  return postfix

def evalPostfix(expression):
  stack = [] #create an empty stack
  for item in expression:
    if is_operand(item):
      stack.append(int(item))
    else:
      if item not in '+-*/%':
        print('Invalid token found: {}'.format(item))
        return -1
      operand2 = stack.pop()
      operand1 = stack.pop()
      if item == '+':
        stack.append(operand1 + operand2)
      elif item == '-':
        stack.append(operand1 - operand2)
      elif item == '*':
        stack.append(operand1 * operand2)
      elif item == '/':
        stack.append(operand1 / operand2)
      else:
        stack.append(operand1 % operand2)
  return stack.pop()

def main():
  if len(sys.argv) <2 :
    buffer  = sys.stdin
  else :
    buffer  = open(sys.argv[1] , "r")

  for line in buffer:
    postfix_list= infix2postfix(line)
    result= evalPostfix(postfix_list)
    print("{} = {}".format(" ".join(postfix_list), result))

if __name__ == '__main__':
  main()
