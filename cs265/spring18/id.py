#!/usr/bin/python2
#By: Fnu Alisha
#Date: 05/04/18
#
#About: This program reads file argument(if given),otherwise it reads stdin. It creates a dictionary for each line in the file by splitting it into 2 fields: id(key) and name(value). Then it prints the entries of dictionary sorted(numerically) by id(key)
#
#Using: python 2.7
#Platform: Linux tux4.cs.drexel.edu 4.4.0-96-generic #119-Ubuntu SMP Tue Sep 12 14:59:54 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

import sys

buffer = open(sys.argv[1], "r") if len(sys.argv) < 2 else sys.stdin

dictionary = { int(line.split()[0]) : ' '.join(line.split()[1:]) for line in buffer}

for key in sorted(dictionary.keys()):
  print "{} {}".format(key, dictionary[key])

import sys

if len(sys.argv) < 2:
  buffer = sys.stdin
else:
  buffer = open( file_name , "r")

dictionary = dict()

for line in buffer:
  id = int(line.split()[0])
  name = ' '.join(line.split()[1:])
  dictionary[id] = name

for key in sorted(dictionary.keys()):
  print "{} {}".format(key, dictionary[key])
