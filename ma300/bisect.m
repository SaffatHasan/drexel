%BISECTION
%This program finds a solution to f(x)=0 given the continuous
%function f on the interval [a,b], where f(a) and f(b) have
%opposite signs
%This program then plots the iterations


a=input('Please enter the left endpoint:')
b=input('Please enter the right endpoint:')
tol=input('Please enter the tolerance:')
maxn=input('Please enter the maximum number of iterations:')

% We will store all iterations in an array p so that we can graph
% them later.

% initialize the array p

for i=1:maxn,
  p(i)=0;
end



i=1;
fa=f(a);
while i<=maxn,

% Compute p(i)

  p(i)=a+(b-a)/2;
  fp=f(p(i));


% Check if within tolerance

  if fp==0
    break;
  elseif (b-a)/2<tol
    break;
  end


% Compute new endpoints a_i,b_i

  if fa*fp>0
    a=p(i);
    fa=fp;
  else
    b=p(i);
  end

  i=i+1;

end

if i==maxn+1
  'Method failed after maxn iterations'
  N=maxn
else
  'After N iterations'
  N=i
  p(N)
end


%Plot the iterations

plot(p(1:N),'b*');
saveas(gcf, 'test.png');

% Plots only the first N terms of p (the rest are zero)
% The 'b*' tells matlab to plot with blue stars
% Type help plot for more info
